/* global AJS */

AJS.$(document).ready(function () {
    console.log("reuse_document_dialog.js");
    var spaceKey = $('meta[name="confluence-space-key"]').attr("content");
    
    $(document).on("click", "#reuse-document-link", function (e) {
        e.preventDefault();
        var dialog = Confluence.Templates.ReuseDocument.dialog();
        $('#main').append(dialog);
        AJS.dialog2("#reuse-document-dialog").show();
    
        // Set default tab
        setTimeout(function () {
            getListSpace('common', $('#ddl_select_common'));
        }, 1000);
        
    });
    
    //挿入
    AJS.$(document).on("click", "#kod-reuse-document-submit-button", function (e) {
        e.preventDefault();
        var curentPageId = $('meta[name="ajs-page-id"]').attr("content");
        var reuseSpaceKey = $('#reuseSpaceKey').val();

        $.ajax({
            url: AJS.contextPath() + "/reusedocument/insert.action",
            data: {curentPageId: curentPageId, reuseSpaceKey: reuseSpaceKey},
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.result === 'false') {
                    AJS.flag({
                        type: 'error',
                        body: data.message
                    });
                } else {
                    location.reload();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                AJS.flag({
                    type: 'error',
                    body: "error occurred!\nXMLHttpRequest : " + XMLHttpRequest.status
                });
            }
        });
    });
    
    //キャンセル
    AJS.$(document).on("click", "#kod-reuse-document-cancel-button", function (e) {
        e.preventDefault();
        AJS.dialog2("#reuse-document-dialog").remove();
    });
    
    // Select Tab
    AJS.$(document).on("click", "#reuse-include-btn", function (e) {
        $('#reuse-document-dialog').find('.panel-include').removeClass('hidden');
        $('#reuse-document-dialog').find('.panel-common').addClass('hidden');
        $('#reuse-document-dialog').find('.panel-data').addClass('hidden');
        setButtonTab("include");
        
        if ($('.is-loaded-include').val() !== 'true') {
            getListSpace('include', $('#ddl_select_include'));
        }
    });
    AJS.$(document).on("click", "#reuse-common-btn", function (e) {
        $('#reuse-document-dialog').find('.panel-include').addClass('hidden');
        $('#reuse-document-dialog').find('.panel-common').removeClass('hidden');
        $('#reuse-document-dialog').find('.panel-data').addClass('hidden');
        setButtonTab("common");
        
        if ($('.is-loaded-common').val() !== 'true') {
            getListSpace('common', $('#ddl_select_common'));
        }
    });
    AJS.$(document).on("click", "#reuse-data-btn", function (e) {
        $('#reuse-document-dialog').find('.panel-include').addClass('hidden');
        $('#reuse-document-dialog').find('.panel-common').addClass('hidden');
        $('#reuse-document-dialog').find('.panel-data').removeClass('hidden');
        setButtonTab("data");
        
        if ($('.is-loaded-data').val() !== 'true') {
            getListSpace('data', $('#ddl_select_data'));
        }
    });
    
    function setButtonTab(mode) {
        if (mode === "include") {
            $('#reuse-document-dialog').find("#reuse-include-btn").addClass("selected-tab");
            $('#reuse-document-dialog').find("#reuse-common-btn").removeClass("selected-tab");
            $('#reuse-document-dialog').find("#reuse-data-btn").removeClass("selected-tab");
        } else if (mode === "common") {
            $('#reuse-document-dialog').find("#reuse-common-btn").addClass("selected-tab");
            $('#reuse-document-dialog').find("#reuse-include-btn").removeClass("selected-tab");
            $('#reuse-document-dialog').find("#reuse-data-btn").removeClass("selected-tab");
        } else if (mode === "data") {
            $('#reuse-document-dialog').find("#reuse-data-btn").addClass("selected-tab");
            $('#reuse-document-dialog').find("#reuse-include-btn").removeClass("selected-tab");
            $('#reuse-document-dialog').find("#reuse-common-btn").removeClass("selected-tab");
        }
    }
    function getListSpace(mode, ddl){        
        ddl.spin();
        ddl.attr('placeholder', '');
        var index;
        var placeholder = "";
        if (mode === "include") {
            index = 1;
            placeholder = AJS.I18n.getText('kod.plugins.reuse.document.preview.document.placeholder');
        } else if (mode === "common") {
            index = 2;
            placeholder = AJS.I18n.getText('kod.plugins.reuse.document.preview.common.placeholder');
        } else if (mode === "data") {
            index = 3;
            placeholder = AJS.I18n.getText('kod.plugins.reuse.document.preview.data.placeholder');
        }
        var flag = $(".is-loaded-" + mode);
        var ddlContainer = "#ddl_select_" + mode + "_container";
        var dataInput = {
            spaceKey: AJS.Meta.get('space-key')
        }

        $.ajax({
            url: AJS.contextPath() + "/rest/manualversion/1.0/documentdata/get/" + index,
            type: "get",
            dataType: "json",
            data: dataInput,
            async: false,
            success: function (res) {
                if (res.length === 0) {
                    alert(AJS.I18n.getText("pageselect.err.msg8"));
                } else {
                    var listItems = res.spaces;
                    listItems.sort(function (a, b) {
                        return a.documentName.localeCompare(b.documentName);
                    });
                    var setHTML = Confluence.Templates.ReuseDocument.listSpace({listItems: listItems, mode: mode, placeholder:placeholder});
                    $(ddlContainer).html(setHTML);
                    flag.val('true');
                    ddl.spinStop();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("error occurred!\nXMLHttpRequest : " + XMLHttpRequest.status);
                AJS.Rte.BookmarkManager.restoreBookmark();
            }
        });
    };
    
    // Change combobox
    AJS.$(document).on("change", "#ddl_select_include", function (e) {
        var spaceKey = $('[name=searchtypeinclude]').val();
        $('#reuseSpaceKey').val(spaceKey);
        showPageTree($('#page_tree_include'), spaceKey);
        $('.preview-img').attr('src', "/rest/manualversion/1.0/space/cover/" + spaceKey);
        $('.preview-img').show();
        
    });
    AJS.$(document).on("change", "#ddl_select_common", function (e) {
        var spaceKey = $('[name=searchtypecommon]').val();
        $('#reuseSpaceKey').val(spaceKey);
        showPageTree($('#page_tree_common'), spaceKey);
        $('.preview-img').attr('src', "/rest/manualversion/1.0/space/cover/" + spaceKey);
        $('.preview-img').show();
    });
    AJS.$(document).on("change", "#ddl_select_data", function (e) {
        var spaceKey = $('[name=searchtypedata]').val();
        $('#reuseSpaceKey').val(spaceKey);
        showPageTree($('#page_tree_data'), spaceKey);
        $('.preview-img').attr('src', "/rest/manualversion/1.0/space/cover/" + spaceKey);
        $('.preview-img').show();
    });
    
    function showPageTree(div, spaceKey) {
        $.ajax({
            type: 'get',
            url: AJS.Meta.get('context-path') + '/rest/wikiworkstheme/1.0/pages/get/' + spaceKey,
            dataType: 'json',
            async: false,
            success: function (data) {
                if (data.result === true) {
                    if (data.page.childs) {
                        var newTree = Confluence.Templates.ReuseDocument.renderPageTree({root: AJS.contextPath(), page: data.page});
                        div.html(newTree);
                        $('#kod-reuse-document-submit-button').prop('disabled', false);
                    }else{
                        div.html("ホームページが見つかりません。");
                        $('#kod-reuse-document-submit-button').prop('disabled', true);
                    }
                }
            },
            error: function () {
            }
        });
    };
    
    $(document).on("click", ".click-zone", function (e) {
        e.preventDefault();
        var parent = $(this).parent();
        var firstUl = parent.find('ul')[0];
        if (parent.hasClass('opened')) {
            parent.removeClass('opened');
            parent.addClass('closed');
            $(firstUl).hide();
        } else {
            parent.removeClass('closed');
            parent.addClass('opened');
            $(firstUl).show();
        }
    });
    
});