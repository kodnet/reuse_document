SELECT 
    c.contentid as id
    , c.title as title
    , c.child_position as position
    , s.spacekey as spacekey
FROM 
    SPACES s
JOIN 
    CONTENT c 
    ON s.spaceid = c.spaceid 
WHERE  c.prevver IS NULL 
    AND (c.contenttype = 'page' or c.contenttype = 'PAGE')
    AND (c.content_status = 'current' or c.content_status = 'CURRENT')
    AND c.parentid = '@PARENT_ID'
ORDER BY 
    (case when c.child_position  is null then 1 else 0 end)
    , c.child_position
    , c.title; 