package jp.co.kodnet.confluence.plugins.util;

import com.atlassian.confluence.servlet.simpledisplay.ConvertedPath;
import com.atlassian.confluence.servlet.simpledisplay.PathConverter;
import java.util.StringTokenizer;

/**
 *
 * @author tampt
 */
public class CustomViewPagePathConverter implements PathConverter {

    private static final String DISPLAY_PAGE_PATH = "/page/include/viewpage.action";

    @Override
    public boolean handles(String simplePath) {
        StringTokenizer st = new StringTokenizer(simplePath, "/");
        boolean isParam = st.countTokens() == 3;
        boolean isPage = st.nextToken().equalsIgnoreCase("page");
        return isParam && isPage;
    }

    @Override
    public ConvertedPath getPath(String simplePath) {
        StringTokenizer st = new StringTokenizer(simplePath, "/");
        String subContext = st.nextToken();
        String spaceKey = st.nextToken();
        String pageId = st.nextToken();

        ConvertedPath path = new ConvertedPath(DISPLAY_PAGE_PATH);
        path.addParameter("spaceKey", spaceKey);
        path.addParameter("pageId", pageId);
        return path;
    }
}
