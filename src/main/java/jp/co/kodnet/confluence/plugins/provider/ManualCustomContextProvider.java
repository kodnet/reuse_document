package jp.co.kodnet.confluence.plugins.provider;

import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.opensymphony.webwork.ServletActionContext;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import jp.co.kodnet.confluence.plugins.common.CommonFunction;
import jp.co.kodnet.confluence.plugins.connector.DocumentKanriConnector;
import jp.co.kodnet.confluence.plugins.factory.ReuseDocumentFactory;

/**
 *
 * @author TamPT
 */
public class ManualCustomContextProvider implements ContextProvider {

    @Override
    public void init(Map<String, String> params) throws PluginParseException {

    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            String toPageId = null;
            String spaceKey = null;
            if (request.getRequestURI().contains("pages/viewpage.action") == true && request.getParameterMap().size() >= 2) {
                toPageId = request.getParameter("pageId");
                spaceKey = request.getParameter("spaceKey");
            } else if (request.getRequestURI().contains("display/page") == true) {
                String[] split = request.getRequestURI().split("/");
                spaceKey = split[split.length - 2];
                toPageId = split[split.length - 1];
            }

            if (toPageId == null || toPageId.isEmpty() == true || spaceKey == null || spaceKey.isEmpty() == true) {
                context.put("body", "");
                return context;
            }

            Object result = DocumentKanriConnector.execute(DocumentKanriConnector.METHOD_MAP_CONTEXT_PROVIDER, Long.valueOf(toPageId));
            if (result == null) {
                context.put("body", "");
                return context;
            }

            Map<String, Object> mapContext = (Map<String, Object>) result;
            SpaceManager spaceManager = ReuseDocumentFactory.getSpaceManager();
            Space space = spaceManager.getSpace(spaceKey);
            Object result1 = DocumentKanriConnector.execute(DocumentKanriConnector.METHOD_MAP_CONTEXT_PROVIDER, space.getHomePage().getId());
            if (result1 == null) {
                context.put("body", "");
                return context;
            }

            Map<String, String> mapVersionPage = (Map<String, String>) mapContext.get("mapVersionPage");
            Map<String, Object> resultMap = (Map<String, Object>) result1;
            Map<String, String> mapVersionSpace = (Map<String, String>) resultMap.get("mapVersionSpace");
            Map<String, String> mapVersionLink = (Map<String, String>) resultMap.get("manualVersions");
            for (Map.Entry<String, String> entry : mapVersionLink.entrySet()) {
                String versionDisplay = entry.getKey();
                for (Map.Entry<String, String> entry1 : mapVersionSpace.entrySet()) {
                    String version = entry1.getKey();
                    String spaceVersion = entry1.getValue();
                    if (spaceVersion.endsWith("_" + versionDisplay) == false){
                        continue;
                    }

                    if (mapVersionPage.containsKey(version) == false) {
                        mapVersionLink.put(versionDisplay, "");
                    } else {
                        String value = mapVersionPage.get(version);
                        mapVersionLink.put(versionDisplay, CommonFunction.getCustomPageUrl(value, spaceVersion.split("_")[0]));
                    }
                }
            }

            SoyTemplateRenderer soyTemplateRenderer = ReuseDocumentFactory.getSoyTemplateRenderer();
            String resultText = soyTemplateRenderer.render("jp.co.kodnet.confluence.plugins.wikiworksmanualversion:wikiworksmanualversion-page-view-resources", "Confluence.Templates.ManualVersionPageView.manualVersionSelect", mapContext);
            context.put("body", "");
        } catch (Exception ex) {
        }

        return context;
    }
}
