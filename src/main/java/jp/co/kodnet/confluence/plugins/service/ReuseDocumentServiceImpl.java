package jp.co.kodnet.confluence.plugins.service;

import com.atlassian.activeobjects.external.ActiveObjects;
import static com.google.common.collect.Lists.newArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import jp.co.kodnet.confluence.plugins.entity.ReuseDocument;
import net.java.ao.Query;

public class ReuseDocumentServiceImpl implements ReuseDocumentService {

    private final ActiveObjects ao;

    public ReuseDocumentServiceImpl(ActiveObjects ao) {
        this.ao = ao;
    }

    @Override
    public List<ReuseDocument> getListReuseDocumentInSpace(String parentSpaceKey) {
        List<ReuseDocument> listReuseDocuments = newArrayList(ao.find(ReuseDocument.class, Query.select()
                .where("PARENT_SPACE_KEY = ?", parentSpaceKey).order("POSITION ASC, ID ASC")));
        return listReuseDocuments;
    }

    @Override
    public List<ReuseDocument> getListReuseDocumentByReuseSpaceKey(String reuseSpaceKey) {
        List<ReuseDocument> listReuseDocuments = newArrayList(ao.find(ReuseDocument.class, Query.select()
                .where("REUSE_SPACE_KEY = ?", reuseSpaceKey).order("POSITION ASC, ID ASC")));
        return listReuseDocuments;
    }

    @Override
    public List<ReuseDocument> getListReuseDocumentInPage(Long parentPageId) {
        List<ReuseDocument> listReuseDocuments = newArrayList(ao.find(ReuseDocument.class, Query.select()
                .where("PARENT_PAGE = ?", parentPageId).order("POSITION ASC, ID ASC")));
        return listReuseDocuments;
    }

    @Override
    public Map<Integer, ReuseDocument> getMapReuseDocumentInPage(Long parentPageId) {
        List<ReuseDocument> listReuseDocuments = this.getListReuseDocumentInPage(parentPageId);
        Map<Integer, ReuseDocument> result = new LinkedHashMap<Integer, ReuseDocument>();
        for (ReuseDocument reuseDocument : listReuseDocuments) {
            if (result.containsKey(reuseDocument.getPosition()) == true) {
                reuseDocument.setPosition(result.size());
                reuseDocument.save();
            }

            result.put(reuseDocument.getPosition(), reuseDocument);
        }

        return result;
    }

    @Override
    public ReuseDocument addReuseDocument(String parentSpaceKey, Long parentPageId, String reuseSpaceKey, Integer position) {
        ReuseDocument reuseDocument = ao.create(ReuseDocument.class);
        reuseDocument.setParentSpaceKey(parentSpaceKey);
        reuseDocument.setParentPage(parentPageId);
        reuseDocument.setPosition(position);
        reuseDocument.setReuseSpaceKey(reuseSpaceKey);
        reuseDocument.save();
        return reuseDocument;
    }

    @Override
    public ReuseDocument updateReuseDocument(String parentSpaceKey, Long parentPageId, String reuseSpaceKey, Integer position) {
        List<ReuseDocument> listReuseDocuments = newArrayList(ao.find(ReuseDocument.class, Query.select()
                .where("PARENT_SPACE_KEY = ? AND REUSE_SPACE_KEY = ?", parentSpaceKey, reuseSpaceKey).order("POSITION ASC, ID ASC")));
        if (!listReuseDocuments.isEmpty()) {
            ReuseDocument reuseDocument = listReuseDocuments.get(0);
            reuseDocument.setParentPage(parentPageId);
            reuseDocument.setPosition(position);
            reuseDocument.save();
            return reuseDocument;
        }
        return null;
    }

    @Override
    public boolean updateReuseSpaceKey(ReuseDocument reuseDocument, String newReuseSpaceKey) {
        reuseDocument.setReuseSpaceKey(newReuseSpaceKey);
        reuseDocument.save();
        return true;
    }

    @Override
    public boolean removeReuseDocument(int id) {
        List<ReuseDocument> listReuseDocuments = newArrayList(ao.find(ReuseDocument.class, Query.select().where("ID = ? ", id)));
        if (listReuseDocuments.isEmpty()) {
            return false;
        }
        ReuseDocument reuseDocument = listReuseDocuments.get(0);
        
        List<ReuseDocument> listReuseDocumentInPageLeft = getListReuseDocumentInPage(reuseDocument.getParentPage());
        for (ReuseDocument leftreuseDocument : listReuseDocumentInPageLeft) {
            int leftPosition = leftreuseDocument.getPosition();
            if (leftPosition > reuseDocument.getPosition()) {
                leftreuseDocument.setPosition(leftPosition - 1);
                leftreuseDocument.save();
            }
        }
        
        ao.delete(reuseDocument);
        return true;
    }
}
