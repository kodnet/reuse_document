package jp.co.kodnet.confluence.plugins.action;

import com.atlassian.confluence.json.JSONAction;
import com.atlassian.confluence.json.json.JsonArray;
import com.atlassian.confluence.json.json.JsonObject;
import com.atlassian.confluence.json.parser.JSONException;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.actions.AbstractPageAwareAction;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionCheckExemptions;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.actions.SpaceAware;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import java.util.ArrayList;
import java.util.List;
import jp.co.kodnet.confluence.plugins.bean.CustomPage;
import jp.co.kodnet.confluence.plugins.common.CommonFunction;
import jp.co.kodnet.confluence.plugins.factory.ReuseDocumentFactory;
import jp.co.kodnet.confluence.plugins.service.ReuseDocumentServicePublic;
import jp.co.kodnet.confluence.plugins.util.PluginLogUtil;
import jp.co.kodnet.confluence.setting.KodSettingManager;
import jp.co.kodnet.confluence.setting.beans.WordExportConfigBean;

/**
 *
 * @author Tam PT
 */
public class CustomChildrenAction extends AbstractPageAwareAction implements SpaceAware, JSONAction {

    private AbstractPage homePage;
    private String spaceKey;
    private String node;
    private Space space;
    private PageManager pageManager;
    private PermissionCheckExemptions permissionCheckExemptions;
    private ReuseDocumentServicePublic reuseDocumentServicePublic;

    public List<CustomPage> getPermittedChildren() throws Exception {
        List<CustomPage> children = new ArrayList<CustomPage>();

        if (!isPermitted()) {
            return children;
        }

        if ("root".equals(this.node) == true) {
            List<Page> pages = filterBasedOnContentPermissions(this.pageManager.getTopLevelPages(getSpace()));
            for (Page page : pages) {
                children.add(CustomPage.createFromReset(page));
            }
            if (this.getPage() == null) {
                this.setPage(this.getSpace().getHomePage());
            }
        } else {
            AbstractPage page = getPage();
            if ("home".equals(this.node) == true) {
                page = this.getHomePage();
            }

            if (page instanceof Page) {
                children = this.reuseDocumentServicePublic.getSortCustomPageChild(page.getId(), true);
            }
        }

        return children;
    }

    public boolean hasPermittedChildren(Page page) {
        if (!isPermitted()) {
            return false;
        }
        if (this.permissionCheckExemptions.isExempt(AuthenticatedUserThreadLocal.get())) {
            return page.hasChildren();
        }
        return this.contentPermissionManager.hasPermittedChildrenIgnoreInheritedPermissions(page, getAuthenticatedUser());
    }

    private List<Page> filterBasedOnContentPermissions(List<Page> pages) {
        if (this.permissionCheckExemptions.isExempt(AuthenticatedUserThreadLocal.get())) {
            return pages;
        }
        ArrayList<Page> ret = new ArrayList<Page>(pages.size());
        for (Page page : pages) {
            if (this.contentPermissionManager.hasContentLevelPermission(AuthenticatedUserThreadLocal.get(), "View", page)) {
                ret.add(page);
            }
        }
        return ret;
    }

    @Override
    public String execute() throws Exception {
        return super.execute();
    }

    @Override
    public String getJSONString() throws JSONException {
        JsonArray array = new JsonArray();
        List<CustomPage> permittedChildren;
        try {
            permittedChildren = getPermittedChildren();
            if (this.getPage() instanceof Page == false) {
                return "[]";
            }

            int position = 0;
            for (CustomPage page : permittedChildren) {
                boolean isReference = this.isReference(page);
                if (isReference == true && isIgnorePage(page) == true) {
                    continue;
                }

                JsonObject jsonObject = getJsonObjectForPage(page, position, isReference == false);
                array.add(jsonObject);
                position++;
            }

            return array.serialize();
        } catch (Exception ex) {
            PluginLogUtil.outputLogException(this.getClass(), ex);
            return "[]";
        }
    }

    public boolean isIgnorePage(CustomPage page) {
        boolean hasLabel = CommonFunction.isPageHasLabel(CommonFunction.LABEL_CONST, page.getId());
        if (hasLabel == true) {
            return true;
        }

        KodSettingManager kodSettingManager = ReuseDocumentFactory.getKodSettingManager();
        WordExportConfigBean wordConfig = kodSettingManager.getWordConfig();
        if (wordConfig.getCover().equalsIgnoreCase(page.getTitle()) == true) {
            return true;
        }

        if (wordConfig.getPreface().equalsIgnoreCase(page.getTitle()) == true) {
            return true;
        }

        if (wordConfig.getAppendix().equalsIgnoreCase(page.getTitle()) == true) {
            return true;
        }

        if (wordConfig.getImpression().equalsIgnoreCase(page.getTitle()) == true) {
            return true;
        }

        return false;
    }

    public JsonObject getJsonObjectForPage(CustomPage page, Integer position, boolean isCurrentSpace) {
        String contextPath = getBootstrapManager().getWebAppContextPath();
        String linkClass = page.isHomePage() && isCurrentSpace == true ? "home-node" : "page-node";
        List<CustomPage> childs = this.reuseDocumentServicePublic.getSortCustomPageChild(page.getId(), false);
        String nodeClass = childs.isEmpty() == true ? "" : "closed";
        if (isCurrentSpace == false && page.isHomePage() == false) {
            nodeClass = nodeClass + " undraggable";
        }

        if (isCurrentSpace == false) {
            nodeClass = nodeClass + " reference-tree reference-node";
        }

        if (page.isHomePage()) {
            nodeClass = nodeClass + " home-node";
        }

        long reuseId = page.getReuseId();
        if (reuseId > 0) {
            nodeClass = nodeClass + " reuse-id-" + reuseId;
        }

        String href = contextPath + page.getUrlPath();
        if (isCurrentSpace == false) {
            href = contextPath + CommonFunction.getCustomPageUrl(String.valueOf(page.getId()), this.getSpaceKey());
        }

        JsonObject jsonObject = (new JsonObject())
                .setProperty("text", page.getDisplayTitle())
                .setProperty("pageId", String.valueOf(page.getId()))
                .setProperty("position", position)
                .setProperty("linkClass", linkClass)
                .setProperty("nodeClass", nodeClass)
                .setProperty("spaceKey", this.getSpaceKey())
                .setProperty("href", href);
        return jsonObject;
    }

    @Override
    public boolean isPermitted() {
        if (getPage() == null) {
            return this.permissionManager.hasPermission(getAuthenticatedUser(), Permission.VIEW, getSpace());
        }

        return this.permissionManager.hasPermission(getAuthenticatedUser(), Permission.VIEW, getPage());
    }

    public void setNode(String node) {
        this.node = node;
    }

    @Override
    public boolean isPageRequired() {
        return false;
    }

    @Override
    public Space getSpace() {
        return (this.space == null) ? super.getSpace() : this.space;
    }

    @Override
    public void setSpace(Space space) {
        this.space = space;
    }

    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    @Override
    public boolean isSpaceRequired() {
        return false;
    }

    public void setPermissionCheckExemptions(PermissionCheckExemptions permissionCheckExemptions) {
        this.permissionCheckExemptions = permissionCheckExemptions;
    }

    public void setReuseDocumentServicePublic(ReuseDocumentServicePublic reuseDocumentServicePublic) {
        this.reuseDocumentServicePublic = reuseDocumentServicePublic;
    }

    public void setSpaceKey(String spaceKey) {
        this.spaceKey = spaceKey;
    }

    @Override
    public String getSpaceKey() {
        if (this.spaceKey == null || this.spaceKey.isEmpty()) {
            this.spaceKey = this.getPage().getSpaceKey();
        }

        return this.spaceKey;
    }

    public boolean isReference(CustomPage page) {
        return CommonFunction.isReference(page, this.getSpaceKey());
    }

    public AbstractPage getHomePage() {
        if (this.homePage == null) {
            Space spaceCurrent = this.spaceManager.getSpace(this.spaceKey);
            if (spaceCurrent != null && spaceCurrent.getHomePage() != null) {
                this.homePage = spaceCurrent.getHomePage();
            } else if (this.getPage() != null && this.getPage().getSpace() != null && this.getPage().getSpace().getHomePage() != null) {
                this.homePage = this.getPage().getSpace().getHomePage();
            } else {
                this.homePage = this.getPage();
            }
        }

        return this.homePage;
    }
}
