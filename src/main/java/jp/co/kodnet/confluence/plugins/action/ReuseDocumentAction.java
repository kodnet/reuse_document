package jp.co.kodnet.confluence.plugins.action;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.opensymphony.xwork.ActionContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jp.co.kodnet.confluence.plugins.common.ActionContextHelper;
import jp.co.kodnet.confluence.plugins.entity.ReuseDocument;
import jp.co.kodnet.confluence.plugins.service.ReuseDocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReuseDocumentAction extends ConfluenceActionSupport implements Beanable {

    private static final Logger log = LoggerFactory.getLogger(ReuseDocumentAction.class);
    private final Map<String, Object> resultMap = new HashMap<>();
    
    private ReuseDocumentService reuseDocumentService ;
    private PageManager pageManager;
    private SpaceManager spaceManager;

    @Override
    public Object getBean() {
        return this.resultMap;
    }
    
    public String doInsert() throws Exception {
        try {
            ActionContext context = ActionContext.getContext();
            Long curentPageId = ActionContextHelper.getFirstParameterValueAsLong(context, "curentPageId", 0);
            String reuseSpaceKey = ActionContextHelper.getFirstParameterValueAsString(context, "reuseSpaceKey");

            Page curentPage = this.pageManager.getPage(curentPageId);
            if (curentPage == null) {
                this.resultMap.put("result", false);
                this.resultMap.put("message", "合冊のページが見つかりません。");
                return SUCCESS;
            }

            Space reuseSpace = this.spaceManager.getSpace(reuseSpaceKey);
            if (reuseSpace == null) {
                this.resultMap.put("result", false);
                this.resultMap.put("message", "分冊のドキュメントが見つかりません。");
                return SUCCESS;
            }

            Long insertInPage = curentPageId;
            Integer position = 0;
            List<ReuseDocument> childs = this.reuseDocumentService.getListReuseDocumentInPage(curentPageId);
            if (curentPage.hasChildren() == true || childs.isEmpty() == false) {
                position = curentPage.getChildren().size() + childs.size();
            }

            this.reuseDocumentService.addReuseDocument(curentPage.getSpaceKey(), insertInPage, reuseSpaceKey, position);
            this.resultMap.put("result", true);
            return SUCCESS;
        } catch (Exception e) {
            log.error("Error finding if user is watching page", e);
            this.resultMap.put("result", false);
            this.resultMap.put("message", e.getMessage());
            return SUCCESS;
        }
    }
    
    public ReuseDocumentService getReuseDocumentService() {
        return reuseDocumentService;
    }

    public void setReuseDocumentService(ReuseDocumentService reuseDocumentService) {
        this.reuseDocumentService = reuseDocumentService;
    }

    public PageManager getPageManager() {
        return pageManager;
    }

    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    public SpaceManager getSpaceManager() {
        return spaceManager;
    }

    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }
}
