package jp.co.kodnet.confluence.plugins.service;

import com.atlassian.activeobjects.tx.Transactional;
import java.util.List;
import java.util.Map;
import jp.co.kodnet.confluence.plugins.entity.ReuseDocument;

@Transactional
public interface ReuseDocumentService {

    List<ReuseDocument> getListReuseDocumentInSpace(String parentSpaceKey);

    List<ReuseDocument> getListReuseDocumentByReuseSpaceKey(String reuseSpaceKey);

    List<ReuseDocument> getListReuseDocumentInPage(Long parentPageId);
    
    Map<Integer, ReuseDocument> getMapReuseDocumentInPage(Long parentPageId);

    ReuseDocument addReuseDocument(String parentSpaceKey, Long parentPageId, String reuseSpaceKey, Integer position);

    ReuseDocument updateReuseDocument(String parentSpaceKey, Long parentPageId, String reuseSpaceKey, Integer position);

    boolean updateReuseSpaceKey(ReuseDocument reuseDocument, String newReuseSpaceKey);

    boolean removeReuseDocument(int id);
}
