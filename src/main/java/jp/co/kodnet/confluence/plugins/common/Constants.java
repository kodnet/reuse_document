package jp.co.kodnet.confluence.plugins.common;

/**
 *
 * @author tampt
 */
public class Constants {

    public static final long FIRST_PAGE = 0L;
    public static final String CUSTOM_DISPLAY_PATH = "/display/page";
    
    public static final String IMAGE_DIRECTORY_PAGE = "imagedirectory";
    public static final String UI_CONTROL_TITLE_PAGE = "uicontrol";

    public static final String IMAGE_DIRECTORY_LABEL = "imagedirectory";
    public static final String UI_CONTROL_TITLE_PAGE_LABEL = "uicontrol";
    public static final String UI_CONTROL_TITLE_PAGE_LABEL_TBD = "tbd";
}
