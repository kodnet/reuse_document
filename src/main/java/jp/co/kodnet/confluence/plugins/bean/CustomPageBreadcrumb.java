package jp.co.kodnet.confluence.plugins.bean;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.util.breadcrumbs.AbstractBreadcrumb;
import com.atlassian.confluence.util.breadcrumbs.Breadcrumb;
import com.atlassian.confluence.util.breadcrumbs.SpaceBreadcrumb;
import jp.co.kodnet.confluence.plugins.common.CommonFunction;

/**
 *
 * @author tampt
 */
public class CustomPageBreadcrumb extends AbstractBreadcrumb {

    private Page page;
    private Breadcrumb parentPage;
    private String spaceKey;

    public CustomPageBreadcrumb(Page page, Breadcrumb parentPage, String spaceKey) {
        super(page.getTitle(), page.getUrlPath());
        this.displayTitle = page.getTitle();
        this.page = page;
        this.parentPage = parentPage;
        boolean isReference = CommonFunction.isReference(page, spaceKey);
        if (isReference == true) {
            this.target = CommonFunction.getCustomPageUrl(page, spaceKey);
        }
    }

    @Override
    public Breadcrumb getParent() {
        if (this.parentPage != null) {
            return parentPage;
        }

        Space space = this.page.getLatestVersion().getSpace();
        return new SpaceBreadcrumb(space);
    }

    public Page getPage() {
        return this.page;
    }
}
