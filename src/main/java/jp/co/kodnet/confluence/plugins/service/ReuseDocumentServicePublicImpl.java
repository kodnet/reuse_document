package jp.co.kodnet.confluence.plugins.service;

import com.atlassian.confluence.core.ContentPermissionManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionCheckExemptions;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.SQLUtils;
import com.atlassian.hibernate.PluginHibernateSessionFactory;
import com.atlassian.spring.container.ContainerManager;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import jp.co.kodnet.confluence.plugins.bean.CustomPage;
import jp.co.kodnet.confluence.plugins.common.CommonFunction;
import jp.co.kodnet.confluence.plugins.connector.TranslationConnector;
import jp.co.kodnet.confluence.plugins.entity.ReuseDocument;
import jp.co.kodnet.confluence.plugins.factory.ReuseDocumentFactory;
import net.sf.hibernate.Session;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

public class ReuseDocumentServicePublicImpl implements ReuseDocumentServicePublic {

    @Override
    public Map<String, Object> getListReuseDocumentInPage(Long parentPageId) {
        Map<String, Object> result = new HashMap<>();
        ReuseDocumentService reuseDocumentService = ReuseDocumentFactory.getReuseDocumentService();

        Map<String, String> lstReuseDocumentKey = new HashMap<>();
        List<ReuseDocument> lstReuseDocument = reuseDocumentService.getListReuseDocumentInPage(parentPageId);
        for (ReuseDocument reuseDocument : lstReuseDocument) {
            lstReuseDocumentKey.put(String.valueOf(reuseDocument.getID()), reuseDocument.getReuseSpaceKey());
        }

        result.put("listReuse", lstReuseDocumentKey);
        return result;
    }

    @Override
    public Map<String, Object> copyReuseDocument(Long cpyFromPageId, Long cpyToPageId, Boolean updateReuse) {
        Map<String, Object> result = new HashMap<>();
        PageManager pageManager = ReuseDocumentFactory.getPageManager();
        try {
            Page cpyToPage = pageManager.getPage(cpyToPageId);
            String cpyToLangeKey = "";
            Object cpyToPageLangObj = TranslationConnector.execute(TranslationConnector.METHOD_GET_LANGUAGE_BY_SPACE_KEY, cpyToPage.getSpaceKey());
            if (cpyToPageLangObj != null) {
                cpyToLangeKey = (String) cpyToPageLangObj;
            }
            // 1 Create reuse data for new document (version-up, copy document, add new language)
            copyReuseDataForNewDocument(cpyToLangeKey, cpyToPage, cpyFromPageId);

            // 2 言語を追加する時、ベース変言語を再使用しているドキュメントの言語は追加言語と同じ場合、追加言語に再使用するように変更
            if (Objects.isNull(updateReuse) || updateReuse) {
                upateReuseDocumentLaguage(cpyFromPageId, cpyToLangeKey, cpyToPage);
            }

            result.put("result", true);

        } catch (Exception e) {
            result.put("result", false);
            result.put("message", e.getMessage());
        }
        return result;
    }

    private void copyReuseDataForNewDocument(String cpyToLangeKey, Page cpyToPage, Long cpyFromPageId) {
        // 1 Create reuse data for new document (version-up, copy document, add new language)
        try {
            ReuseDocumentService reuseDocumentService = ReuseDocumentFactory.getReuseDocumentService();
            List<ReuseDocument> lstReuseDocument = reuseDocumentService.getListReuseDocumentInPage(cpyFromPageId);
            if (!lstReuseDocument.isEmpty()) {
                for (ReuseDocument reuseDocument : lstReuseDocument) {
                    String reuseSpaceKey = reuseDocument.getReuseSpaceKey();

                    Map<String, Object> resultMap;
                    Object reflection = TranslationConnector.execute(TranslationConnector.METHOD_GET_ALL_INFO_LANGUAGE_BY_SPACE_KEY, reuseDocument.getReuseSpaceKey());
                    if (reflection != null) {
                        resultMap = (Map<String, Object>) reflection;
                        if (resultMap.get("languages") != null) {
                            Map<String, Object> languageMaps = (Map<String, Object>) resultMap.get("languages");
                            if (languageMaps.get(cpyToLangeKey) != null) {
                                Map<String, Object> properties = (Map<String, Object>) languageMaps.get(cpyToLangeKey);
                                reuseSpaceKey = (String) properties.get("spaceKey");
                            }
                        }
                    }
                    reuseDocumentService.addReuseDocument(cpyToPage.getSpaceKey(), cpyToPage.getId(), reuseSpaceKey, reuseDocument.getPosition());
                }
            }

        } catch (Exception e) {

        }
    }

    private void upateReuseDocumentLaguage(Long cpyFromPageId, String cpyToLangeKey, Page cpyToPage) {
        try {
            PageManager pageManager = ReuseDocumentFactory.getPageManager();
            ReuseDocumentService reuseDocumentService = ReuseDocumentFactory.getReuseDocumentService();
            if (StringUtils.isBlank(cpyToLangeKey)) {
                return;
            }
            // get List REUSE_DOCUEMT By ByReuseSpaceKey 
            Page fromSpace = pageManager.getPage(cpyFromPageId);
            List<ReuseDocument> lstReuseDocumentByReuseSpaceKey = reuseDocumentService.getListReuseDocumentByReuseSpaceKey(fromSpace.getSpaceKey());
            for (ReuseDocument reuseDocument : lstReuseDocumentByReuseSpaceKey) {
                // get laguae fo PARENT_SPACE_KEY
                String langOfParentSpace = "";
                Object langOfParentSpaceObj = TranslationConnector.execute(TranslationConnector.METHOD_GET_LANGUAGE_BY_SPACE_KEY, reuseDocument.getParentSpaceKey());
                if (langOfParentSpaceObj != null) {
                    langOfParentSpace = (String) langOfParentSpaceObj;
                }
                // If Language of langOfParentSpace = to_langkey
                if (!StringUtils.isBlank(langOfParentSpace) && langOfParentSpace.equals(cpyToLangeKey)) {
                    // update  REUSE_SPACE_KEY= spaceKeyOf(①to_langkey)
                    reuseDocumentService.updateReuseSpaceKey(reuseDocument, cpyToPage.getSpaceKey());
                }
            }

        } catch (Exception e) {

        }
    }

    @Override
    public List<Page> getSortPageChild(Long parentPageId) {
        List<Page> lstPages = new ArrayList<Page>();
        PageManager pageManager = ReuseDocumentFactory.getPageManager();
        SpaceManager spaceManager = ReuseDocumentFactory.getSpaceManager();
        PermissionCheckExemptions permissionCheckExemptions = (PermissionCheckExemptions) ContainerManager.getInstance().getContainerContext().getComponent("permissionCheckExemptions");
        ContentPermissionManager contentPermissionManager = (ContentPermissionManager) ContainerManager.getInstance().getContainerContext().getComponent("contentPermissionManager");
        Page parentPage = pageManager.getPage(parentPageId);
        if (parentPage == null) {
            return lstPages;
        }

        List<Page> childrens = new ArrayList<Page>();
        if (permissionCheckExemptions.isExempt(AuthenticatedUserThreadLocal.get())) {
            childrens = parentPage.getSortedChildren();
        } else {
            childrens = contentPermissionManager.getPermittedChildren(parentPage, AuthenticatedUserThreadLocal.get());
        }

        ReuseDocumentService reuseDocumentService = jp.co.kodnet.confluence.plugins.factory.ReuseDocumentFactory.getReuseDocumentService();
        Map<Integer, ReuseDocument> reuseDocuments = reuseDocumentService.getMapReuseDocumentInPage(parentPageId);
        int position = 0;
        for (int i = 0; i < childrens.size();) {
            if (reuseDocuments.containsKey(position) == true) {
                ReuseDocument reuseDocument = reuseDocuments.get(position);
                Space spaceRefrence = spaceManager.getSpace(reuseDocument.getReuseSpaceKey());
                if (spaceRefrence != null && spaceRefrence.getHomePage() != null) {
                    Page reusePageHome = (Page) spaceRefrence.getHomePage().clone();
                    reusePageHome.getProperties().setStringProperty("ReuseId", String.valueOf(reuseDocument.getID()));
                    lstPages.add(reusePageHome);
                    reuseDocuments.remove(position);
                    position++;
                    continue;
                }
            }

            lstPages.add(childrens.get(i));
            position++;
            i++;
        }

        for (Map.Entry<Integer, ReuseDocument> entry : reuseDocuments.entrySet()) {
            Space spaceRefrence = spaceManager.getSpace(entry.getValue().getReuseSpaceKey());
            if (spaceRefrence != null && spaceRefrence.getHomePage() != null) {
                Page reusePageHome = (Page) spaceRefrence.getHomePage().clone();
                reusePageHome.getProperties().setStringProperty("ReuseId", String.valueOf(entry.getValue().getID()));
                lstPages.add(reusePageHome);
            }
        }

        return lstPages;
    }

    @Override
    public List<Long> getAncestorPageId(Long pageId, String spaceKey) {
        List<Long> pageIds = new ArrayList<Long>();
        PageManager pageManager = ReuseDocumentFactory.getPageManager();
        Page page = pageManager.getPage(pageId);
        if (page == null) {
            return pageIds;
        }

        List<Page> ancestorPages = CommonFunction.getAncestor(page, spaceKey);
        for (Page ancestorPage : ancestorPages) {
            pageIds.add(ancestorPage.getId());
        }

        return pageIds;
    }

    @Override
    public List<String> getListReuseDocumentInSpace(String spaceKey) {
        List<String> reuseSpaceKey = new ArrayList<>();
        ReuseDocumentService reuseDocumentService = ReuseDocumentFactory.getReuseDocumentService();
        List<ReuseDocument> lstReuseDocument = reuseDocumentService.getListReuseDocumentInSpace(spaceKey);
        for (ReuseDocument reuseDocument : lstReuseDocument) {
            reuseSpaceKey.add(reuseDocument.getReuseSpaceKey());
        }
        return reuseSpaceKey;
    }

    @Override
    public List<CustomPage> getSortCustomPageChild(Long parentPageId, boolean includeReuse) {
        List<CustomPage> resultData = new ArrayList<CustomPage>();
        PageManager pageManager = ReuseDocumentFactory.getPageManager();
        SpaceManager spaceManager = ReuseDocumentFactory.getSpaceManager();
        Page parentPage = pageManager.getPage(parentPageId);
        if (parentPage == null) {
            return resultData;
        }

        List<CustomPage> childrens = new ArrayList<CustomPage>();
        Statement ps = null;
        ResultSet rs = null;

        try {
            PluginHibernateSessionFactory sessionFactory = (PluginHibernateSessionFactory) ContainerManager.getInstance().getContainerContext().getComponent("pluginHibernateSessionFactory");
            Session session = sessionFactory.getSession();
            Connection conn = session.connection();
            InputStream in = getClass().getResourceAsStream("/db/childpage.sql");
            String query = IOUtils.toString(in, "UTF-8");
            query = query.replace("@PARENT_ID", String.valueOf(parentPageId));
            ps = conn.createStatement();
            rs = ps.executeQuery(query);
            while (rs.next()) {
                CustomPage page = CustomPage.createFromReset(rs);
                childrens.add(page);
            }

            if (includeReuse == false) {
                return childrens;
            }

            ReuseDocumentService reuseDocumentService = ReuseDocumentFactory.getReuseDocumentService();
            Map<Integer, ReuseDocument> reuseDocuments = reuseDocumentService.getMapReuseDocumentInPage(parentPageId);
            if (reuseDocuments.isEmpty() == true){
                return childrens;
            }

            int position = 0;
            for (int i = 0; i < childrens.size();) {
                if (reuseDocuments.containsKey(position) == true) {
                    ReuseDocument reuseDocument = reuseDocuments.get(position);
                    Space spaceRefrence = spaceManager.getSpace(reuseDocument.getReuseSpaceKey());
                    if (spaceRefrence != null && spaceRefrence.getHomePage() != null) {
                        CustomPage reusePageHome = CustomPage.createFromReset(spaceRefrence.getHomePage());
                        reusePageHome.setReuseId(reuseDocument.getID());
                        resultData.add(reusePageHome);
                        reuseDocuments.remove(position);
                        position++;
                        continue;
                    }
                }

                resultData.add(childrens.get(i));
                position++;
                i++;
            }

            for (Map.Entry<Integer, ReuseDocument> entry : reuseDocuments.entrySet()) {
                Space spaceRefrence = spaceManager.getSpace(entry.getValue().getReuseSpaceKey());
                if (spaceRefrence != null && spaceRefrence.getHomePage() != null) {
                    CustomPage reusePageHome = CustomPage.createFromReset(spaceRefrence.getHomePage());
                    reusePageHome.setReuseId(entry.getValue().getID());
                    resultData.add(reusePageHome);
                }
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        } finally {
            SQLUtils.closeResultSetQuietly(rs);
        }

        return resultData;
    }
}
