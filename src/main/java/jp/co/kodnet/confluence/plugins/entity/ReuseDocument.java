package jp.co.kodnet.confluence.plugins.entity;
import net.java.ao.Entity;
import net.java.ao.Preload;

@Preload
public interface ReuseDocument extends Entity {

    String getParentSpaceKey();

    void setParentSpaceKey(String spaceKey);
    
    Long getParentPage();

    void setParentPage(Long PageId);
     
    Integer getPosition();

    void setPosition(Integer position);
    
    String getReuseSpaceKey();

    void setReuseSpaceKey(String spaceKey);
}
