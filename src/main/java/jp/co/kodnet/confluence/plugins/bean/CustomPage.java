package jp.co.kodnet.confluence.plugins.bean;

import com.atlassian.confluence.pages.Page;
import java.sql.ResultSet;

/**
 *
 * @author TamPT
 */
public class CustomPage {

    private long Id;
    private String title;
    private Integer position;
    private boolean isHome;
    private long reuseId;
    private String spaceKey;

    public long getId() {
        return Id;
    }

    public void setId(long Id) {
        this.Id = Id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public boolean isHomePage() {
        return isHome;
    }

    public void setIsHome(boolean isHome) {
        this.isHome = isHome;
    }

    public long getReuseId() {
        return reuseId;
    }

    public void setReuseId(long reuseId) {
        this.reuseId = reuseId;
    }

    public String getSpaceKey() {
        return spaceKey;
    }

    public void setSpaceKey(String spaceKey) {
        this.spaceKey = spaceKey;
    }

    public String getUrlPath() {
        return "/pages/viewpage.action?pageId=" + this.getId();
    }

    public String getDisplayTitle() {
        return title;
    }

    public static CustomPage createFromReset(ResultSet rs) throws Exception {
        CustomPage page = new CustomPage();
        page.setId(rs.getLong("id"));
        page.setTitle(rs.getString("title"));
        page.setPosition(rs.getInt("position"));
        page.setSpaceKey(rs.getString("spacekey"));
        page.setIsHome(false);
        page.setReuseId(0L);
        return page;
    }

    public static CustomPage createFromReset(Page p) throws Exception {
        CustomPage page = new CustomPage();
        page.setId(p.getId());
        page.setTitle(p.getTitle());
        page.setPosition(p.getPosition());
        page.setIsHome(p.isHomePage());
        page.setSpaceKey(p.getSpaceKey());
        page.setReuseId(0L);
        return page;
    }
}
