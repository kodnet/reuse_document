package jp.co.kodnet.confluence.plugins.action;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.event.Evented;
import com.atlassian.confluence.event.events.ConfluenceEvent;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.actions.CommentAware;
import com.atlassian.confluence.pages.actions.ViewPageAction;
import com.atlassian.confluence.plugin.descriptor.web.DefaultWebInterfaceContext;
import com.atlassian.confluence.plugin.descriptor.web.WebInterfaceContext;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.breadcrumbs.Breadcrumb;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.velocity.htmlsafe.HtmlSafe;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jp.co.kodnet.confluence.plugins.common.CommonFunction;
import jp.co.kodnet.confluence.plugins.util.CustomViewPageFormat;

/**
 *
 * @author tampt
 */
public class CustomViewPageAction extends ViewPageAction implements CommentAware, Evented<ConfluenceEvent> {

    private XhtmlContent xhtmlContent;
    private Space space;
    private String spaceKey;
    private long pageId;

    @Override
    public String execute() throws Exception {
        if (this.pageId > 0) {
            Page page = this.pageManager.getPage(this.pageId);
            this.setPage(page);
        }

        if (this.spaceKey != null && this.spaceKey.isEmpty() == false) {
            this.setSpace(this.spaceManager.getSpace(this.spaceKey));
        }

        return super.execute();
    }

    @Override
    @HtmlSafe
    public String getPageXHtmlContent() {
        //TODO reflection manual version
        super.setPageContent(this.getPage().getBodyAsString());
        AbstractPage page = (AbstractPage) this.xhtmlContent.convertWikiBodyToStorage(getPage());
        String bodyAsString = CustomViewPageFormat.convertStorageFormat(this.getSpaceKey(), this.getPage(), this.getPageContent());
        String pageXHtmlContent = this.viewRenderer.render(bodyAsString, new DefaultConversionContext(page.toPageContext()));
        return pageXHtmlContent;
    }

    @Override
    public void setXhtmlContent(XhtmlContent xhtmlContent) {
        this.xhtmlContent = xhtmlContent;
        super.setXhtmlContent(xhtmlContent);
    }

    @Override
    public String getEditingUser() {
        if (this.getAuthenticatedUser() != null) {
            return this.getAuthenticatedUser().getName();
        } else {
            return null;
        }
    }

    @Override
    public WebInterfaceContext getWebInterfaceContext() {
        DefaultWebInterfaceContext result = (DefaultWebInterfaceContext) super.getWebInterfaceContext();
        List<Label> labels = getPage() == null ? Collections.emptyList() : getPage().getLabelsForDisplay(getAuthenticatedUser());
        result.setParameter("labels", labels);
        if (getClass().equals(CustomViewPageAction.class)) {
            result.setParameter("viewMode", CommonFunction.isReference(this.getPage(), this.getSpaceKey()) == false);
            result.setParameter("space", this.getSpace());
            result.setParameter("spaceKey", this.getSpaceKey());
            result.setParameter("isCustomView", this.getIsCustomView());
            result.setParameter("breadcrumb", this.getBreadcrumbs());
            result.setParameter("rootPageId", this.getSpace().getHomePage().getId());
        }

        return result;
    }

    @Override
    public Comment getComment() {
        return super.getComment();
    }

    @Override
    public void setComment(Comment cmnt) {
        super.setComment(cmnt);
    }

    @Override
    public ConfluenceEvent getEventToPublish(String result) {
        return super.getEventToPublish(result);
    }

    @Override
    public String getSpaceKey() {
        if (this.spaceKey == null || this.spaceKey.isEmpty()) {
            this.spaceKey = this.getPage().getSpaceKey();
        }

        return this.spaceKey;
    }

    public void setSpaceKey(String spaceKey) {
        this.spaceKey = spaceKey;
    }

    @Override
    public long getPageId() {
        return pageId;
    }

    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    public Space getSpace() {
        return space;
    }

    public void setSpace(Space space) {
        this.space = space;
    }

    public boolean getIsCustomView() {
        return true;
    }

    public String getBreadcrumbs() {
        Map<String, Object> map = new HashMap<String, Object>();
        List<Breadcrumb> breadcrumbs = CommonFunction.getBreadcrumb(this.getPage(), this.getSpaceKey());
        map.put("breadcrumbs", breadcrumbs);
        map.put("action", this);
        map.put("generalUtil", new GeneralUtil());
        map.put("commonUtil", new CommonFunction());
        map.put("contextPath", this.settingsManager.getGlobalSettings().getBaseUrl());
        return VelocityUtils.getRenderedTemplate("/view/include/breadcrumbs.vm", map);
    }
}
