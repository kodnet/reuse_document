package jp.co.kodnet.confluence.plugins.connector;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import jp.co.kodnet.confluence.plugins.factory.ReuseDocumentFactory;
import org.apache.commons.beanutils.MethodUtils;

/**
 *
 * @author TamPT
 */
public class TranslationConnector {

    public static final String TRANSLATION_PLUGIN_KEY = "jp.co.kodnet.confluence.plugins.KollaboTranslation";
    public static final String TRANSLATION_PUBLIC_SERVICE = "translationPublicService";

    public static final String METHOD_GET_LANGUAGE = "getLanguageSuport";
    public static final String METHOD_DO_ADD_NEW_VALIDATE = "doAddNewValidate";
    public static final String METHOD_DO_ADD_NEW_TRANSLATION = "doAddNewTranslation";
    public static final String METHOD_DO_DELETE_TRANSLATION = "doDeleteTranslation";
    public static final String METHOD_DO_ADD_VERSION_TRANSLATION = "doAddVersionTranslation";
    public static final String METHOD_GET_BASE_LANGUAGE = "getBaseLanguage";
    public static final String METHOD_GET_BASE_PAGE_ID = "getBasePageId";
    public static final String METHOD_GET_FROM_PAGE_ID = "getFromPageId";
    public static final String METHOD_GET_BASE_PAGE_ID_FROM_VERSION = "getBasePageIdFromVersion";
    public static final String METHOD_GET_PAGE_ID_BY_BASE_PAGE_ID = "getPageIdByBasePageId";
    public static final String METHOD_GET_FROM_SPACE_KEY_BY_SPACE_KEY = "getSpaceKeyFromBySpaceKey";
    public static final String METHOD_GET_PAGE_ID_BY_LANGUAGE = "getPageIdByLanguage";
    public static final String METHOD_GET_LANGUAGE_BY_PAGE_ID = "getLanguageByPageId";
    public static final String METHOD_GET_LANGUAGE_BY_SPACE_KEY = "getLanguageBySpaceKey";
    public static final String METHOD_GET_LANGUAGE_INFO_BY_SPACE_KEY = "getLanguageInfoBySpaceKey";
    public static final String METHOD_GET_LANGUAGE_BASE_SCROLL_BY_SPACE_KEY = "getLanguageBaseScrollTransBySpaceKey";
    public static final String METHOD_GET_LANGUAGE_AND_SPACE_KEY_BY_SPACE_KEY = "getLanguageAndSpaceKey";
    public static final String METHOD_GET_BASE_SPACE_KEY_BY_SPACE_KEY = "getBaseSpaceKeyBySpaceKey";
    public static final String METHOD_GET_LANGUAGES_BY_FROM_LANGUAGE = "getListTargetLanguageByFromBaseLanguage";
    public static final String METHOD_GET_TRANSLATION_MEMORY_PLUGIN_KEY = "getTranslationMemoryPluginKey";
    public static final String METHOD_UPDATE_LINK_VISUAL_ATTACHMENT= "changeLinkImageVisualIndex";
    public static final String METHOD_UPDATE_VERSION_HONYAKU = "updateVersionHonyakuKanri";
    public static final String METHOD_DO_ADD_NEW_VERSION_HONYAKU_AND_CREATE_TARGET_LANGUAGE = "createVersionHonyakuKanri";
    public static final String METHOD_GET_TRANSACTION_COPY_TRANSLATION_VERSION = "getTransactionCopyTranslationVersion";
    public static final String METHOD_UPDATE_TRANS_PAGE_HISTORY= "updateTransPageHistory";
    public static final String METHOD_MAP_CONTEXT_PROVIDER = "getMapContextProvider";
    public static final String METHOD_GET_ALL_INFO_LANGUAGE_BY_SPACE_KEY = "getAllInfoLanguageBySpaceKey";
    public static final String METHOD_GET_PAGE_HISTORY_BY_FROM_PAGE_ID ="getTransPageHistoryByFromPageId";

    public static Plugin getEnabledPlugin() throws Exception {
        try {

            PluginAccessor pluginAccessor = ReuseDocumentFactory.getPluginAccessor();
            Plugin plugin = pluginAccessor.getEnabledPlugin(TRANSLATION_PLUGIN_KEY);
            if (plugin == null) {
                return null;
            }

            return plugin;
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean hasEnabledPlugin() throws Exception {
        Plugin plugin = getEnabledPlugin();
        return plugin != null;
    }

    public static Object execute(String method, Object... param) throws Exception {
        try {
            Plugin plugin = getEnabledPlugin();
            if (plugin == null) {
                return null;
            }

            ModuleDescriptor moduleDescriptor = plugin.getModuleDescriptor(TRANSLATION_PUBLIC_SERVICE);
            if (moduleDescriptor == null) {
                return null;
            }

            Object value = MethodUtils.invokeMethod(moduleDescriptor.getModule(), method, param);
            if (value == null) {
                return null;
            }

            return value;
        } catch (Exception e) {
            return null;
        }
    }
}
