package jp.co.kodnet.confluence.plugins.util;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.links.linktypes.AbstractPageLink;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import jp.co.kodnet.confluence.plugins.common.CommonFunction;
import jp.co.kodnet.confluence.plugins.factory.ReuseDocumentFactory;
import jp.co.kodnet.confluence.xml.XmlDocumentUtil;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 *
 * @author tampt
 */
public class CustomViewPageFormat {

    public static String convertStorageFormat(String spaceKeyCurrent, AbstractPage pageCurrent, String bodyAsString) {
        try {
            bodyAsString = convertAcLinkPage(spaceKeyCurrent, pageCurrent, bodyAsString);
            bodyAsString = convertIncludeMacro(spaceKeyCurrent, pageCurrent, bodyAsString);
        } catch (Exception ex) {
        }

        return bodyAsString;
    }

    public static String convertAcLinkPage(String spaceKeyCurrent, AbstractPage pageCurrent, String bodyAsString) throws Exception {
        PageManager pageManager = ReuseDocumentFactory.getPageManager();
        Document document = XmlDocumentUtil.convertStringToXml(bodyAsString, Boolean.TRUE);
        Element root = document.getDocumentElement();
        List<Element> elements = XmlDocumentUtil.getElementByTagName(root, "ac:link");
        for (Element element : elements) {
            Element acImage = XmlDocumentUtil.getFirstElementByTagName(element, "ac:image");
            if (acImage != null) {
                continue;
            }

            Element acRiUrl = XmlDocumentUtil.getFirstElementByTagNameAndAttr(element, "ri:url", "ri:value");
            if (acRiUrl != null) {
                continue;
            }

            //Get anchor value
            String anchorValue = "";
            String textLink = pageCurrent.getTitle();
            if (element.hasAttribute("ac:anchor")) {
                anchorValue = element.getAttribute("ac:anchor");
                textLink = anchorValue;
            }

            //Get ri:page
            Element riPage = XmlDocumentUtil.getFirstElementByTagName(element, "ri:page");
            Boolean isContinues = false;
            Integer count = 0;
            Node parentNode = riPage;
            do {
                if (parentNode == null) {
                    break;
                }
                parentNode = parentNode.getParentNode();
                count++;
                Element parent = (Element) parentNode;
                if (parent.getTagName().equals("ac:structured-macro") && parent.hasAttribute("ac:name") && parent.getAttribute("ac:name").equals("include-template")) {
                    isContinues = true;
                }
                if (parent.getTagName().equals("ac:structured-macro") && parent.hasAttribute("ac:name") && parent.getAttribute("ac:name").equals("children")) {
                    isContinues = true;
                }
                if (parent.getTagName().equals("ac:structured-macro") && parent.hasAttribute("ac:name") && parent.getAttribute("ac:name").equals("include")) {
                    isContinues = true;
                }
            } while (!isContinues && count < 3);

            if (isContinues) {
                continue;
            }

            String pageTitle = pageCurrent.getDisplayTitle();
            String spaceKey = pageCurrent.getSpaceKey();
            if (riPage != null) {
                pageTitle = riPage.getAttribute("ri:content-title");
                if (riPage.hasAttribute("ri:space-key")) {
                    spaceKey = riPage.getAttribute("ri:space-key");
                }
            }

            Page page = pageManager.getPage(spaceKey, pageTitle);
            if (page == null) {
                continue;
            }

            if (page.getSpaceKey().equals(pageCurrent.getSpaceKey()) == false) {
                continue;
            }

            if (anchorValue != null && !anchorValue.isEmpty()) {
                pageTitle = pageTitle + "#" + anchorValue;
            }

            Element acTextLink = XmlDocumentUtil.getFirstElementByTagName(element, "ac:plain-text-link-body");
            Element linkBody = XmlDocumentUtil.getFirstElementByTagName(element, "ac:link-body");
            if (acTextLink != null) {
                if (acTextLink.getFirstChild() instanceof CharacterData) {
                    CharacterData cd = (CharacterData) acTextLink.getFirstChild();
                    pageTitle = cd.getData();
                    textLink = cd.getData();
                }
            } else if (linkBody != null) {
                pageTitle = linkBody.getTextContent();
            }

            Element newLink = createLinkElement(document, spaceKeyCurrent, page, pageTitle, anchorValue, textLink);
            element.getParentNode().insertBefore(newLink, element);
            element.getParentNode().removeChild(element);
        }

        return XmlDocumentUtil.convertXmlToString(document.getDocumentElement(), true, true);
    }

    private static Element createLinkElement(Document doc, String spaceKey, AbstractPage page, String pageTitle, String anchor, String textLink) throws Exception {
        Element newLink = doc.createElement("a");
        String href = CommonFunction.getCustomPageUrl(page, spaceKey);
        if (anchor != null && !anchor.isEmpty()) {
            ConversionContext conversionContext = new DefaultConversionContext(page.toPageContext());
            anchor = AbstractPageLink.generateAnchor(conversionContext.getPageContext(), anchor);
            if (!anchor.contains("?")) {
                href = href + "#" + anchor;
            } else {
                href = href + "?" + anchor;
            }
        }

        if (textLink.isEmpty()) {
            textLink = pageTitle;
        }
        newLink.setAttribute("href", href);
        newLink.setTextContent(textLink);
        return newLink;
    }

    //Include macro
    public static String convertIncludeMacro(String spaceKeyCurrent, AbstractPage currentPage, String bodyAsString) throws Exception {
        Map<String, String> mapInclude = new LinkedHashMap<String, String>();
        bodyAsString = convertIncludeMacro(spaceKeyCurrent, currentPage, bodyAsString, mapInclude);
        for (Map.Entry<String, String> entry : mapInclude.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            bodyAsString = bodyAsString.replace(String.format("<div id=\"%s\">%s</div>", key, key), value);
        }
        return bodyAsString;
    }

    public static String convertIncludeMacro(String spaceKeyCurrent, AbstractPage currentPage, String bodyAsString, Map<String, String> mapInclude) throws Exception {
        PageManager pageManager = ReuseDocumentFactory.getPageManager();
        bodyAsString = addParamPageToMacro(currentPage.getSpaceKey(), currentPage.getTitle(), currentPage.getIdAsString(), bodyAsString);
        Document document = XmlDocumentUtil.convertStringToXml(bodyAsString, Boolean.TRUE);
        Element root = document.getDocumentElement();
        List<Element> elements = XmlDocumentUtil.getElementsByTagNameAndAttr(root, "ac:structured-macro", "ac:name", "include");
        Random rd = new Random(99999999L);
        for (Element element : elements) {
            String macroIdRefer = null;
            //Get ri:page
            Element riPage = XmlDocumentUtil.getFirstElementByTagName(element, "ri:page");
            if (riPage == null) {
                continue;
            }
            Element macroIdReferElement = XmlDocumentUtil.getFirstElementByTagNameAndAttr(element, "ac:parameter", "ac:name", "macroId");
            if (macroIdReferElement != null) {
                macroIdRefer = macroIdReferElement.getTextContent();
            }
            String pageTitle = riPage.getAttribute("ri:content-title");
            String spaceKey = currentPage.getSpaceKey();
            if (riPage.hasAttribute("ri:space-key")) {
                spaceKey = riPage.getAttribute("ri:space-key");
            }

            Page includePage = pageManager.getPage(spaceKey, pageTitle);
            if (includePage == null) {
                continue;
            }

            String key = String.valueOf(rd.nextLong());
            key = includePage.getIdAsString() + key;
            String includeBodyAsString = convertStorageFormat(spaceKeyCurrent, includePage, includePage.getBodyAsString());
            includeBodyAsString = addSpaceKeyOnRipage(includePage.getSpaceKey(), includeBodyAsString);
            mapInclude.put(key, includeBodyAsString);
            Element newElement = document.createElement("div");
            newElement.setAttribute("id", key);
            newElement.setTextContent(key);
            element.getParentNode().insertBefore(newElement, element);
            element.getParentNode().removeChild(element);
        }

        return XmlDocumentUtil.convertXmlToString(document.getDocumentElement(), true, true);
    }

    public static String addSpaceKeyOnRipage(String spaceKey, String bodyAsString) throws Exception {
        Document document = XmlDocumentUtil.convertStringToXml(bodyAsString, Boolean.TRUE);
        Element root = document.getDocumentElement();
        List<Element> elements = XmlDocumentUtil.getElementByTagName(root, "ri:page");
        for (Element element : elements) {
            if (element.hasAttribute("ri:space-key")) {
                continue;
            }
            element.setAttribute("ri:space-key", spaceKey);
        }
        return XmlDocumentUtil.convertXmlToString(document.getDocumentElement(), true, true);
    }

    public static String addParamPageToMacro(String spaceKey, String pageTitle, String pageId, String bodyAsString) throws Exception {
        Document document = XmlDocumentUtil.convertStringToXml(bodyAsString, Boolean.TRUE);
        Element root = document.getDocumentElement();
        List<Element> elements = new ArrayList<Element>();
        elements.addAll(XmlDocumentUtil.getElementsByTagNameAndAttr(root, "ac:structured-macro", "ac:name", "visualIndexTool"));
        elements.addAll(XmlDocumentUtil.getElementsByTagNameAndAttr(root, "ac:structured-macro", "ac:name", "video-macro"));
        for (Element element : elements) {
            Element param = document.createElement("ac:parameter");
            param.setAttribute("ac:name", "pageId");
            param.setTextContent(pageId);
            element.appendChild(param);

            param = document.createElement("ac:parameter");
            param.setAttribute("ac:name", "page");
            param.setTextContent(pageTitle);
            element.appendChild(param);

            param = document.createElement("ac:parameter");
            param.setAttribute("ac:name", "space");
            param.setTextContent(spaceKey);
            element.appendChild(param);
        }

        return XmlDocumentUtil.convertXmlToString(document.getDocumentElement(), true, true);
    }

}
