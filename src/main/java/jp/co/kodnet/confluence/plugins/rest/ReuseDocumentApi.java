package jp.co.kodnet.confluence.plugins.rest;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import jp.co.kodnet.confluence.plugins.service.ReuseDocumentService;
import jp.co.kodnet.confluence.plugins.service.ReuseDocumentServicePublic;

@Path("/")
public class ReuseDocumentApi {

    private ReuseDocumentService reuseDocumentService;
    private ReuseDocumentServicePublic reuseDocumentServicePublic;
    private PageManager pageManager;
    private SpaceManager spaceManager;
    private final Map<String, String> mapLabelNonDisplay = new HashMap<String, String>() {
        {
            put("tbd", "tbd");
            put("uicontrol", "uicontrol");
            put("imagedirectory", "imagedirectory");
        }
    };

    public ReuseDocumentApi(SpaceManager spaceManager, PageManager pageManager, ReuseDocumentService reuseDocumentService, ReuseDocumentServicePublic reuseDocumentServicePublic) {
        this.spaceManager = spaceManager;
        this.pageManager = pageManager;
        this.reuseDocumentService = reuseDocumentService;
        this.reuseDocumentServicePublic = reuseDocumentServicePublic;
    }

    public JsonObject toExceptionJson(Exception ex) {
        JsonObject json = new JsonObject();
        json.addProperty("result", false);
        json.addProperty("message", ex.getMessage());
        return json;
    }

    @GET
    @Path("/pages/update")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @AnonymousAllowed
    public Response getReuseDocumentPageTree(
            @QueryParam("parentId") Long parentId,
            @QueryParam("pageIds") List<Long> childPageIds,
            @QueryParam("position") Integer position,
            @QueryParam("spaceKey") String spaceKey) {
        try {
            JsonObject result = new JsonObject();
            Space parentSpace = this.spaceManager.getSpace(spaceKey);
            Page parentPage = null;
            if (parentId == 0 || parentId == null) {
                parentPage = parentSpace.getHomePage();
            } else {
                parentPage = this.pageManager.getPage(parentId);
            }

            if (parentPage == null) {
                result.addProperty("result", Boolean.FALSE);
                return Response.ok(result.toString()).build();
            }

            for (int i = 0; i < childPageIds.size(); i++) {
                Page child = this.pageManager.getPage(childPageIds.get(i));
                this.reuseDocumentService.updateReuseDocument(parentSpace.getKey(), parentPage.getId(), child.getSpaceKey(), i);
            }

            result.addProperty("result", Boolean.TRUE);
            return Response.ok(result.toString()).build();
        } catch (Exception ex) {
            return Response.ok(this.toExceptionJson(ex)).build();
        }
    }

    @GET
    @Path("/pages/get/{spaceKey}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @AnonymousAllowed
    public Response getReuseDocumentPageTree(@PathParam("spaceKey") String spaceKey) {
        try {
            JsonObject result = new JsonObject();
            Space space = spaceManager.getSpace(spaceKey);
            if (space == null) {
                return Response.ok(result.toString()).build();
            }

            result.addProperty("result", Boolean.TRUE);
            result.add("page", getPageTree(space.getHomePage()));
            return Response.ok(result.toString()).build();
        } catch (Exception ex) {
            return Response.ok(this.toExceptionJson(ex)).build();
        }
    }
    
    @POST
    @Path("/delete")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @AnonymousAllowed
    public Response deleteReuseDocument(String data) {
        try {
            JsonObject result = new JsonObject();
            JsonParser jsonParser = new JsonParser();
            JsonObject jsonParam = jsonParser.parse(data).getAsJsonObject();                        
            String reuseId = jsonParam.get("reuseId").getAsString();            
            this.reuseDocumentService.removeReuseDocument(Integer.parseInt(reuseId));
            result.addProperty("result", Boolean.TRUE);
            return Response.ok(result.toString()).build();
        } catch (Exception ex) {
            return Response.ok(this.toExceptionJson(ex)).build();
        }
    }
    
    @GET
    @Path("/pages/ancestor/{spaceKey}/{pageId}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @AnonymousAllowed
    public Response getAncestorPageId(@DefaultValue("0") @PathParam("pageId") Long pageId, @PathParam("spaceKey") String spaceKey) {
        try {
            JsonObject result = new JsonObject();
            if (pageId == 0L) {
                result.addProperty("result", Boolean.TRUE);
                result.addProperty("ancestorIds", "[]");
                return Response.ok(result.toString()).build();
            }

            List<Long> pageIds = reuseDocumentServicePublic.getAncestorPageId(pageId, spaceKey);
            Gson gson = new Gson();
            result.addProperty("result", Boolean.TRUE);
            result.add("ancestorIds", gson.toJsonTree(pageIds));
            return Response.ok(result.toString()).build();
        } catch (Exception ex) {
            return Response.ok(this.toExceptionJson(ex)).build();
        }
    }

    private JsonObject getPageTree(Page page) {
        JsonObject result = new JsonObject();
        if (page == null) {
            return result;
        }

        boolean hasDisplay = checkDisplay(page);
        if (hasDisplay == false) {
            return result;
        }

        JsonArray childs = new JsonArray();
        result.addProperty("pageId", page.getIdAsString());
        result.addProperty("pageTitle", page.getDisplayTitle());
        result.addProperty("lv", 0);
        childs = this.getPageTreeChilds(childs, page, 1);
        result.add("childs", childs);
        return result;
    }

    private JsonArray getPageTreeChilds(JsonArray result, Page pageParent, int lv) {
        JsonArray childs = null;
        JsonObject jsonPage = null;
        List<Page> lstPage = pageParent.getSortedChildren();
        for (Page page : lstPage) {
            boolean hasExport = checkDisplay(page);
            if (hasExport == false) {
                continue;
            }

            childs = new JsonArray();
            jsonPage = new JsonObject();
            jsonPage.addProperty("pageId", page.getIdAsString());
            jsonPage.addProperty("pageTitle", page.getDisplayTitle());
            jsonPage.addProperty("lv", lv);
            childs = this.getPageTreeChilds(childs, page, lv + 1);
            jsonPage.add("childs", childs);
            result.add(jsonPage);
        }
        return result;
    }

    private Boolean checkDisplay(Page page) {

//        if(page.getTitle().equals(kodSettingManager.getWordConfig().getCover())){
//            return false;
//        }
//        if(page.getTitle().equals(kodSettingManager.getWordConfig().getAppendix())){
//            return false;
//        }
//        if(page.getTitle().equals(kodSettingManager.getWordConfig().getImpression())){
//            return false;
//        }
//        if(page.getTitle().equals(kodSettingManager.getWordConfig().getPreface())){
//            return false;
//        }
        List<Label> labels = page.getLabels();
        for (Label label : labels) {
            boolean hasLabelNoExport = mapLabelNonDisplay.get(label.getName()) != null;
            if (hasLabelNoExport == true) {
                return false;
            }
        }
        return true;
    }
}
