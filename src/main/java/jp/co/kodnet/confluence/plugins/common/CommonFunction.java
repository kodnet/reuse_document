package jp.co.kodnet.confluence.plugins.common;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.breadcrumbs.Breadcrumb;
import com.atlassian.confluence.util.breadcrumbs.SpaceBreadcrumb;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import jp.co.kodnet.confluence.plugins.bean.CustomPage;
import jp.co.kodnet.confluence.plugins.bean.CustomPageBreadcrumb;
import jp.co.kodnet.confluence.plugins.entity.ReuseDocument;
import jp.co.kodnet.confluence.plugins.factory.ReuseDocumentFactory;
import jp.co.kodnet.confluence.plugins.service.ReuseDocumentService;

public class CommonFunction {

    public static List<String> LABEL_CONST = new ArrayList<String>();

    static {
        LABEL_CONST.add(Constants.IMAGE_DIRECTORY_LABEL);
        LABEL_CONST.add(Constants.UI_CONTROL_TITLE_PAGE_LABEL);
        LABEL_CONST.add(Constants.UI_CONTROL_TITLE_PAGE_LABEL_TBD);
    }

    public static boolean isPageHasLabel(String label, Page page) {
        List<String> labelChecks = new ArrayList<String>();
        labelChecks.add(label);
        return isPageHasLabel(labelChecks, page);
    }

    public static boolean isPageHasLabel(List<String> labelChecks, Page page) {
        if (page.isDeleted() == true || page.isDraft() == true || page.isLatestVersion() == false) {
            return false;
        }

        List<Label> labels = page.getLabels();
        for (Label l : labels) {
            if (labelChecks.contains(l.getName()) == true || labelChecks.contains(l.getDisplayTitle()) == true) {
                return true;
            }
        }

        return false;
    }

    public static boolean isPageHasLabel(List<String> labelChecks, Long pageId) {
        PageManager pageManager = ReuseDocumentFactory.getPageManager();
        Page page = pageManager.getPage(pageId);
        if (page == null) {
            return false;
        }

        return isPageHasLabel(labelChecks, page);
    }

    public static boolean isReference(AbstractPage page, String spaceKey) {
        return page.getSpaceKey().equalsIgnoreCase(spaceKey) == false;
    }

    public static boolean isReference(CustomPage page, String spaceKey) {
        return page.getSpaceKey().equalsIgnoreCase(spaceKey) == false;
    }

    public static String getCustomPageUrl(AbstractPage page, String spaceKey) {
        return getCustomPageUrl(String.valueOf(page.getId()), spaceKey);
    }

    public static String getCustomPageUrl(String pageId, String spaceKey) {
        return Constants.CUSTOM_DISPLAY_PATH + "/" + spaceKey + "/" + pageId;
    }

    public static List<Page> getAncestor(AbstractPage abstractPage, String spaceKey) {
        ReuseDocumentService reuseDocumentService = ReuseDocumentFactory.getReuseDocumentService();
        List<Page> result = new ArrayList<Page>();
        if (abstractPage instanceof Page == false) {
            return result;
        }

        Page page = (Page) abstractPage;
        boolean isReference = CommonFunction.isReference(page, spaceKey);
        if (isReference == true) {
            List<ReuseDocument> reuseDocuments = reuseDocumentService.getListReuseDocumentInSpace(spaceKey);
            if (reuseDocuments.isEmpty() == false && reuseDocuments.get(0).getParentPage() != null) {
                Long pageId = reuseDocuments.get(0).getParentPage();
                if (pageId.longValue() == Constants.FIRST_PAGE) {
                    SpaceManager spaceManager = ReuseDocumentFactory.getSpaceManager();
                    Space space = spaceManager.getSpace(reuseDocuments.get(0).getParentSpaceKey());
                    if (space != null && space.getHomePage() != null) {
                        result.add(space.getHomePage());
                    }
                } else {
                    PageManager pageManager = ReuseDocumentFactory.getPageManager();
                    Page pageAfter = pageManager.getPage(pageId);
                    if (pageAfter != null) {
                        result.addAll(pageAfter.getAncestors());
                    }

                    result.add(pageAfter);
                }
            }
        }

        result.addAll(page.getAncestors());
        return result;
    }

    public static List<Breadcrumb> getBreadcrumb(AbstractPage abstractPage, String spaceKey) {
        List<Page> pages = CommonFunction.getAncestor(abstractPage, spaceKey);
        List<Breadcrumb> result = new ArrayList<Breadcrumb>();
        SpaceManager spaceManager = ReuseDocumentFactory.getSpaceManager();
        Space space = spaceManager.getSpace(spaceKey);
        if (space != null) {
            result.add(new SpaceBreadcrumb(space));
        }

        Breadcrumb parent = null;
        for (int i = 0; i < pages.size(); i++) {
            Breadcrumb current = new CustomPageBreadcrumb(pages.get(i), parent, spaceKey);
            result.add(current);
            parent = current;
        }

        return result;
    }

    public static List<Breadcrumb> getGlobalEllipsisCrumbs(List<Breadcrumb> trail) {
        CommonFunction common = new CommonFunction();
        return common.getEllipsisCrumbs(trail);
    }

    public List<Breadcrumb> getEllipsisCrumbs(List<Breadcrumb> trail) {
        if (trail.size() < 4) {
            return Collections.emptyList();
        }

        LinkedList<Breadcrumb> ellipsedCrumbs = Lists.newLinkedList(trail);
        ellipsedCrumbs.removeFirst();
        ellipsedCrumbs.removeLast();
        if (ellipsedCrumbs.size() < 2) {
            return Collections.emptyList();
        }
        return ellipsedCrumbs;
    }
}
