package jp.co.kodnet.confluence.plugins.factory;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.core.ContentPermissionManager;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.SpaceLabelManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.links.LinkManager;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.CommentManager;
import com.atlassian.confluence.pages.FileUploadManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.security.administrators.PermissionsAdministratorBuilder;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.themes.StylesheetManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.util.longrunning.LongRunningTaskManager;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.GroupManager;
import com.atlassian.user.User;
import java.util.Locale;
import jp.co.kodnet.confluence.language.KodLanguageManager;
import jp.co.kodnet.confluence.plugins.entity.ReuseDocument;
import jp.co.kodnet.confluence.plugins.service.ReuseDocumentService;
import jp.co.kodnet.confluence.setting.KodSettingManager;
import net.java.ao.DatabaseProvider;
import net.java.ao.EntityManager;

/**
 *
 * @author TamPT
 */
public class ReuseDocumentFactory {

    // <editor-fold defaultstate="collapsed" desc="variable">
    private static SpaceManager spaceManager;
    private static PageManager pageManager;
    private static SpaceLabelManager spaceLabelManager;
    private static LabelManager labelManager;
    private static AttachmentManager attachmentManager;
    private static BootstrapManager bootstrapManager;
    private static SettingsManager settingsManager;
    private static LongRunningTaskManager longRunningTaskManager;
    private static LinkManager linkManager;
    private static PermissionManager permissionManager;
    private static CommentManager commentManager;
    private static SpacePermissionManager spacePermissionManager;
    private static ContentPermissionManager contentPermissionManager;
    private static StylesheetManager stylesheetManager;
    private static BandanaManager bandanaManager;
    private static FileUploadManager fileUploadManager;
    private static ActiveObjects activeObjects;
    private static EntityManager entityManager;
    private static DatabaseProvider databaseProvider;
    private static PermissionsAdministratorBuilder permissionsAdministratorBuilder;

    private static PluginAccessor pluginAccessor;
    private static UserAccessor userAccessor;
    protected static GroupManager groupManager;
    private static LocaleManager localeManager;
    private static I18NBeanFactory i18NBeanFactory;
    private static I18NBean i18NBean;
    private static TransactionTemplate transactionTemplate;
    private static XhtmlContent xhtmlContent;
    private static SoyTemplateRenderer soyTemplateRenderer;
    private static KodLanguageManager kodLanguageManager;
    private static KodSettingManager kodSettingManager;
    private static ReuseDocumentService reuseDocumentService;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="constructor">
    public ReuseDocumentFactory(SpaceManager spaceManager,
            PageManager pageManager,
            SpaceLabelManager spaceLabelManager,
            LabelManager labelManager,
            AttachmentManager attachmentManager,
            BootstrapManager bootstrapManager,
            SettingsManager settingsManager,
            LongRunningTaskManager longRunningTaskManager,
            LinkManager linkManager,
            PermissionManager permissionManager,
            CommentManager commentManager,
            SpacePermissionManager spacePermissionManager,
            ContentPermissionManager contentPermissionManager,
            StylesheetManager stylesheetManager,
            BandanaManager bandanaManager,
            FileUploadManager fileUploadManager,
            ActiveObjects activeObjects,
            PluginAccessor pluginAccessor,
            UserAccessor userAccessor,
            GroupManager groupManager,
            LocaleManager localeManager,
            I18NBeanFactory i18NBeanFactory,
            TransactionTemplate transactionTemplate,
            XhtmlContent xhtmlContent,
            SoyTemplateRenderer soyTemplateRenderer,
            KodLanguageManager kodLanguageManager,
            KodSettingManager kodSettingManager,
            ReuseDocumentService reuseDocumentService) {
        ReuseDocumentFactory.spaceManager = spaceManager;
        ReuseDocumentFactory.pageManager = pageManager;
        ReuseDocumentFactory.spaceLabelManager = spaceLabelManager;
        ReuseDocumentFactory.labelManager = labelManager;
        ReuseDocumentFactory.attachmentManager = attachmentManager;
        ReuseDocumentFactory.bootstrapManager = bootstrapManager;
        ReuseDocumentFactory.settingsManager = settingsManager;
        ReuseDocumentFactory.longRunningTaskManager = longRunningTaskManager;
        ReuseDocumentFactory.linkManager = linkManager;
        ReuseDocumentFactory.permissionManager = permissionManager;
        ReuseDocumentFactory.commentManager = commentManager;
        ReuseDocumentFactory.spacePermissionManager = spacePermissionManager;
        ReuseDocumentFactory.contentPermissionManager = contentPermissionManager;
        ReuseDocumentFactory.stylesheetManager = stylesheetManager;
        ReuseDocumentFactory.bandanaManager = bandanaManager;
        ReuseDocumentFactory.fileUploadManager = fileUploadManager;
        ReuseDocumentFactory.activeObjects = activeObjects;
        ReuseDocumentFactory.pluginAccessor = pluginAccessor;
        ReuseDocumentFactory.userAccessor = userAccessor;
        ReuseDocumentFactory.groupManager = groupManager;
        ReuseDocumentFactory.localeManager = localeManager;
        ReuseDocumentFactory.i18NBeanFactory = i18NBeanFactory;
        ReuseDocumentFactory.transactionTemplate = transactionTemplate;
        ReuseDocumentFactory.xhtmlContent = xhtmlContent;
        ReuseDocumentFactory.soyTemplateRenderer = soyTemplateRenderer;
        ReuseDocumentFactory.kodLanguageManager = kodLanguageManager;
        ReuseDocumentFactory.kodSettingManager = kodSettingManager;
        ReuseDocumentFactory.reuseDocumentService = reuseDocumentService;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="set/get">
    public static SpaceManager getSpaceManager() {
        return spaceManager;
    }

    public static void setSpaceManager(SpaceManager spaceManager) {
        ReuseDocumentFactory.spaceManager = spaceManager;
    }

    public static PageManager getPageManager() {
        return pageManager;
    }

    public static void setPageManager(PageManager pageManager) {
        ReuseDocumentFactory.pageManager = pageManager;
    }

    public static SpaceLabelManager getSpaceLabelManager() {
        return spaceLabelManager;
    }

    public static void setSpaceLabelManager(SpaceLabelManager spaceLabelManager) {
        ReuseDocumentFactory.spaceLabelManager = spaceLabelManager;
    }

    public static LabelManager getLabelManager() {
        return labelManager;
    }

    public static void setLabelManager(LabelManager labelManager) {
        ReuseDocumentFactory.labelManager = labelManager;
    }

    public static AttachmentManager getAttachmentManager() {
        return attachmentManager;
    }

    public static void setAttachmentManager(AttachmentManager attachmentManager) {
        ReuseDocumentFactory.attachmentManager = attachmentManager;
    }

    public static BootstrapManager getBootstrapManager() {
        return bootstrapManager;
    }

    public static void setBootstrapManager(BootstrapManager bootstrapManager) {
        ReuseDocumentFactory.bootstrapManager = bootstrapManager;
    }

    public static SettingsManager getSettingsManager() {
        return settingsManager;
    }

    public static void setSettingsManager(SettingsManager settingsManager) {
        ReuseDocumentFactory.settingsManager = settingsManager;
    }

    public static LongRunningTaskManager getLongRunningTaskManager() {
        return longRunningTaskManager;
    }

    public static void setLongRunningTaskManager(LongRunningTaskManager longRunningTaskManager) {
        ReuseDocumentFactory.longRunningTaskManager = longRunningTaskManager;
    }

    public static PermissionsAdministratorBuilder getPermissionsAdministratorBuilder() {
        if (permissionsAdministratorBuilder == null) {
            permissionsAdministratorBuilder = (PermissionsAdministratorBuilder) ContainerManager.getInstance().getContainerContext().getComponent("permissionsAdministratorBuilder");
        }
        return permissionsAdministratorBuilder;
    }

    public static LinkManager getLinkManager() {
        return linkManager;
    }

    public static void setLinkManager(LinkManager linkManager) {
        ReuseDocumentFactory.linkManager = linkManager;
    }

    public static PermissionManager getPermissionManager() {
        return permissionManager;
    }

    public static void setPermissionManager(PermissionManager permissionManager) {
        ReuseDocumentFactory.permissionManager = permissionManager;
    }

    public static CommentManager getCommentManager() {
        return commentManager;
    }

    public static void setCommentManager(CommentManager commentManager) {
        ReuseDocumentFactory.commentManager = commentManager;
    }

    public static SpacePermissionManager getSpacePermissionManager() {
        return spacePermissionManager;
    }

    public static void setSpacePermissionManager(SpacePermissionManager spacePermissionManager) {
        ReuseDocumentFactory.spacePermissionManager = spacePermissionManager;
    }

    public static ContentPermissionManager getContentPermissionManager() {
        return contentPermissionManager;
    }

    public static void setContentPermissionManager(ContentPermissionManager contentPermissionManager) {
        ReuseDocumentFactory.contentPermissionManager = contentPermissionManager;
    }

    public static StylesheetManager getStylesheetManager() {
        return stylesheetManager;
    }

    public static void setStylesheetManager(StylesheetManager stylesheetManager) {
        ReuseDocumentFactory.stylesheetManager = stylesheetManager;
    }

    public static BandanaManager getBandanaManager() {
        return bandanaManager;
    }

    public static FileUploadManager getFileUploadManager() {
        return fileUploadManager;
    }

    public static void setFileUploadManager(FileUploadManager fileUploadManager) {
        ReuseDocumentFactory.fileUploadManager = fileUploadManager;
    }

    public static void setBandanaManager(BandanaManager bandanaManager) {
        ReuseDocumentFactory.bandanaManager = bandanaManager;
    }

    public static ActiveObjects getActiveObjects() {
        return activeObjects;
    }

    public static EntityManager getEntityManager() {
        if (entityManager == null) {
            ReuseDocument temp = getActiveObjects().create(ReuseDocument.class);
            entityManager = temp.getEntityManager();
        }

        return entityManager;
    }

    public static DatabaseProvider getDatabaseProvider() {
        if (databaseProvider == null) {
            databaseProvider = getEntityManager().getProvider();
        }

        return databaseProvider;
    }

    public static PluginAccessor getPluginAccessor() {
        return pluginAccessor;
    }

    public static void setPluginAccessor(PluginAccessor pluginAccessor) {
        ReuseDocumentFactory.pluginAccessor = pluginAccessor;
    }

    public static UserAccessor getUserAccessor() {
        return userAccessor;
    }

    public static void setUserAccessor(UserAccessor userAccessor) {
        ReuseDocumentFactory.userAccessor = userAccessor;
    }

    public static GroupManager getGroupManager() {
        return groupManager;
    }

    public static void setGroupManager(GroupManager aGroupManager) {
        groupManager = aGroupManager;
    }

    public static LocaleManager getLocaleManager() {
        return localeManager;
    }

    public static void setLocaleManager(LocaleManager localeManager) {
        ReuseDocumentFactory.localeManager = localeManager;
    }

    public static I18NBeanFactory getI18NBeanFactory() {
        return i18NBeanFactory;
    }

    public static void setI18NBeanFactory(I18NBeanFactory i18NBeanFactory) {
        ReuseDocumentFactory.i18NBeanFactory = i18NBeanFactory;
    }

    public static I18NBean getI18NBean() {
        User user = AuthenticatedUserThreadLocal.get();
        if (user == null) {
            i18NBean = (I18NBean) ContainerManager.getInstance().getContainerContext().getComponent("i18NBean");
        } else {
            Locale locale = ReuseDocumentFactory.getLocaleManager().getLocale(user);
            i18NBean = i18NBeanFactory.getI18NBean(locale);
        }
        return i18NBean;
    }

    public static void setI18NBean(I18NBean i18NBean) {
        ReuseDocumentFactory.i18NBean = i18NBean;
    }

    public static TransactionTemplate getTransactionTemplate() {
        return transactionTemplate;
    }

    public static void setTransactionTemplate(TransactionTemplate transactionTemplate) {
        ReuseDocumentFactory.transactionTemplate = transactionTemplate;
    }

    public static XhtmlContent getXhtmlContent() {
        return xhtmlContent;
    }

    public static void setXhtmlContent(XhtmlContent xhtmlContent) {
        ReuseDocumentFactory.xhtmlContent = xhtmlContent;
    }

    public static SoyTemplateRenderer getSoyTemplateRenderer() {
        return soyTemplateRenderer;
    }

    public static void setSoyTemplateRenderer(SoyTemplateRenderer soyTemplateRenderer) {
        ReuseDocumentFactory.soyTemplateRenderer = soyTemplateRenderer;
    }

    public static KodLanguageManager getKodLanguageManager() {
        return kodLanguageManager;
    }

    public static void setKodLanguageManager(KodLanguageManager kodLanguageManager) {
        ReuseDocumentFactory.kodLanguageManager = kodLanguageManager;
    }

    public static KodSettingManager getKodSettingManager() {
        return kodSettingManager;
    }

    public static void setKodSettingManager(KodSettingManager kodSettingManager) {
        ReuseDocumentFactory.kodSettingManager = kodSettingManager;
    }

    public static ReuseDocumentService getReuseDocumentService() {
        return reuseDocumentService;
    }

    public static void setReuseDocumentService(ReuseDocumentService reuseDocumentService) {
        ReuseDocumentFactory.reuseDocumentService = reuseDocumentService;
    }
    // </editor-fold>
}
