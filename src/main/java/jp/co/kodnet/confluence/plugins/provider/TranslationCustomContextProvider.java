package jp.co.kodnet.confluence.plugins.provider;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.opensymphony.webwork.ServletActionContext;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import jp.co.kodnet.confluence.plugins.connector.TranslationConnector;

/**
 *
 * @author TamPT
 */
public class TranslationCustomContextProvider implements ContextProvider {

    @Override
    public void init(Map<String, String> params) throws PluginParseException {

    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            String toPageId = null;
            String spaceKey = null;
            if (request.getRequestURI().contains("pages/viewpage.action") == true && request.getParameterMap().size() >= 2) {
                toPageId = request.getParameter("pageId");
                spaceKey = request.getParameter("spaceKey");
            } else if (request.getRequestURI().contains("display/page") == true) {
                String[] split = request.getRequestURI().split("/");
                spaceKey = split[split.length - 2];
                toPageId = split[split.length - 1];
            }

            if (toPageId == null || toPageId.isEmpty() == true || spaceKey == null || spaceKey.isEmpty() == true) {
                context.put("body", "");
                return context;
            }

            Object result = TranslationConnector.execute(TranslationConnector.METHOD_MAP_CONTEXT_PROVIDER, Long.valueOf(toPageId));
            if (result == null) {
                context.put("body", "");
                return context;
            }

            context = (Map<String, Object>) result;
            context.put("isReuseDocument", true);
        } catch (Exception ex) {
        }

        return context;
    }
}
