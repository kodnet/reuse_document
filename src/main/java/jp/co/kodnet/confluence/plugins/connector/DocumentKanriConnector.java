package jp.co.kodnet.confluence.plugins.connector;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import jp.co.kodnet.confluence.plugins.factory.ReuseDocumentFactory;
import org.apache.commons.beanutils.MethodUtils;

/**
 *
 * @author TamPT
 */
public class DocumentKanriConnector {

    public static final String DOCUMENT_PLUGIN_KEY = "jp.co.kodnet.confluence.plugins.wikiworksmanualversion";
    public static final String DOCUMENT_PUBLIC_SERVICE = "manualVersionPublicService";

    public static final String METHOD_GET_DOCUMENT_MANAGEMENT = "getManualManagementByTranslationKey";
    public static final String METHOD_GET_DOCUMENT_VERSION = "getManualVersion";
    public static final String METHOD_DO_DELETE_TRANSLATION = "deleteTranslation";
    public static final String METHOD_CHECK_IS_SPACE_COMPARE = "checkIsSpaceCompare";
    public static final String METHOD_CHECK_IS_SPACE_HONYAKU = "checkIsSpaceManualStartHonyakukanri";
    public static final String METHOD_ADD_NEW_TRANSLATION = "addNewTranslation";
    public static final String METHOD_GET_DOCUMENT_BY_SPACE = "getManualVersionBySpaceKey";
    public static final String METHOD_CHECK_IMAGE_DIRECTORY_FROM_SPACE_KEY = "checkImageDirectoryFromSpaceKey";
    public static final String METHOD_CHECK_IS_IMAGE_IN_BASE_LANG_FROM_SPACE_KEY = "checkIsImageInBaseLangFromSpaceKey";
    public static final String METHOD_CHECK_IS_IMAGE_TO_HOME_PAGE_LANG_FROM_SPACE_KEY = "checkImageToHomePage";
    public static final String METHOD_MAP_CONTEXT_PROVIDER = "getMapContextProvider";

    public static Plugin getEnabledPlugin() throws Exception {
        try {

            PluginAccessor pluginAccessor = ReuseDocumentFactory.getPluginAccessor();
            Plugin plugin = pluginAccessor.getEnabledPlugin(DOCUMENT_PLUGIN_KEY);
            if (plugin == null) {
                return null;
            }

            return plugin;
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean hasEnabledPlugin() throws Exception {
        Plugin plugin = getEnabledPlugin();
        return plugin != null;
    }

    public static Object execute(String method, Object... param) throws Exception {
        try {
            Plugin plugin = getEnabledPlugin();
            if (plugin == null) {
                return null;
            }

            ModuleDescriptor moduleDescriptor = plugin.getModuleDescriptor(DOCUMENT_PUBLIC_SERVICE);
            if (moduleDescriptor == null) {
                return null;
            }

            Object value = MethodUtils.invokeMethod(moduleDescriptor.getModule(), method, param);
            if (value == null) {
                return null;
            }

            return value;
        } catch (Exception e) {
            return null;
        }
    }
}
