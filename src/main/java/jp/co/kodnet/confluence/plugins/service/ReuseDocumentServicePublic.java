package jp.co.kodnet.confluence.plugins.service;

import com.atlassian.activeobjects.tx.Transactional;
import com.atlassian.confluence.pages.Page;
import java.util.List;
import java.util.Map;
import jp.co.kodnet.confluence.plugins.bean.CustomPage;

@Transactional
public interface ReuseDocumentServicePublic {

    Map<String, Object> getListReuseDocumentInPage(Long parentPageId);

    Map<String, Object> copyReuseDocument(Long cpyFromPageId, Long cpyToPageId, Boolean updateReuse);

    List<Page> getSortPageChild(Long parentPageId);

    List<Long> getAncestorPageId(Long pageId, String spaceKey);

    List<String> getListReuseDocumentInSpace(String spaceKey);

    List<CustomPage> getSortCustomPageChild(Long parentPageId, boolean includeReuse);
}
